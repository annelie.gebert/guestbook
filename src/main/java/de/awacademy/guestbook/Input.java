package de.awacademy.guestbook;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Input {

    // Id generieren
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String titel;
    private String description;

    @OneToMany(mappedBy = "input")
    private List<Comment> comments;

    // getter und Setter

    public List<Comment> getComments() {
        return comments;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
