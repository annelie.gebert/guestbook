package de.awacademy.guestbook;

public class CommentDTO {

    private String text;

    public CommentDTO() {
    }

    public CommentDTO(String text) {
        this.text = text;
    }

    // getter und setter

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
