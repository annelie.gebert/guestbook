package de.awacademy.guestbook;

//DTO = Data Transfer Object

public class InputDTO {

    private String titel;
    private String description;

    public InputDTO(){}

    public InputDTO(String titel, String description) {
        this.titel = titel;
        this.description = description;
    }

    // ##### Getter und Setter ######

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
