package de.awacademy.guestbook;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class GuestbookController {
    InsertRepository insertRepository;
    CommentRepository commentRepository;

    public GuestbookController(InsertRepository insertRepository, CommentRepository commentRepository) {
        this.insertRepository = insertRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/")
    public String showIndex(Model model) {

        List<Input> inputList = insertRepository.findAll();
        model.addAttribute("inputList", inputList);

        return "index";
    }

    //##### Input ###########################################################################
    @GetMapping("createInput")
    public String showCreateInput(Model model) {

        InputDTO input = new InputDTO("","");
        model.addAttribute("input", input);

        return "createInput";
    }

    @PostMapping("/formCreateInput")
    public String formCreateInput(@ModelAttribute InputDTO inputDTO) {

        // neuer Input wird erzeugt, der in die Datenbank eingetragen wird
        Input input = new Input();
        input.setDescription(inputDTO.getDescription());
        input.setTitel(inputDTO.getTitel());
        insertRepository.save(input);

        return "redirect:/";
    }
    //##### Comment ###########################################################################
    @GetMapping("/createComment")
    public String showCreateComment(@RequestParam int inputId, Model model) {
        CommentDTO comment = new CommentDTO("");

        model.addAttribute("comment", comment);
        model.addAttribute("inputId", inputId);

        return "createComment";
    }

    @PostMapping("/submitComment")
    public String submitComment(@ModelAttribute CommentDTO commentDTO,@RequestParam int inputId) {

        Comment comment = new Comment();
        comment.setText(commentDTO.getText());

        Input input = insertRepository.findById(inputId).get();
        comment.setInput(input);
        commentRepository.save(comment);

        return "redirect:/";
    }
}
