package de.awacademy.guestbook;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InsertRepository extends JpaRepository<Input, Integer> {
}
